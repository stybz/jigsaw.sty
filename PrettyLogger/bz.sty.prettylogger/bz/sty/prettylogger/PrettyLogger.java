package bz.sty.prettylogger;

import bz.sty.logger.Logger;
/**
 * From
 *
 * @author Mihail Stoynov
 */
public class PrettyLogger extends Logger {
    public void log(String message) {
        System.out.println("PrettyLogger: "+message);
    }
}
