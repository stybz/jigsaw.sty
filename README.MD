# Modules in Java? Finally!

This talk is for a very important new feature in Java SE 9.
Code named Jigsaw, this feature modularizes the Java SE platform.
The **coolest thing** we do here is to create a **custom JRE**

## Source code 

[https://bitbucket.org/stybz/jigsaw.sty/src/](https://bitbucket.org/stybz/jigsaw.sty/src/)

### Presentation

[http://www.slideshare.net/mihailstoynov/modules-in-java-finally-openjdk-jigsaw](http://www.slideshare.net/mihailstoynov/modules-in-java-finally-openjdk-jigsaw)

### Video

[https://www.youtube.com/watch?v=W5LeNPtPrqw](https://www.youtube.com/watch?v=W5LeNPtPrqw)