package bz.sty.pluggablelogger.spi;

import bz.sty.pluggablelogger.PluggableLogger;

/**
 * From
 *
 * @author Mihail Stoynov
 */
public abstract class PluggableLoggerProvider {
    protected PluggableLoggerProvider() { }

    public abstract PluggableLogger getPluggableLogger();
}
