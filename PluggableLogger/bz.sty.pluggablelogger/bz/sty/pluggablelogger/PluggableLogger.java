package bz.sty.pluggablelogger;

import java.io.Closeable;
import java.util.Iterator;
import java.util.ServiceLoader;

import bz.sty.pluggablelogger.spi.PluggableLoggerProvider;
/**
 * From
 *
 * @author Mihail Stoynov
 */
public abstract class PluggableLogger {

    public static PluggableLogger get() {
        ServiceLoader<PluggableLoggerProvider> sl
                = ServiceLoader.load(PluggableLoggerProvider.class);
        Iterator<PluggableLoggerProvider> iter = sl.iterator();
        if (!iter.hasNext())
            throw new RuntimeException("No service providers found!");
        PluggableLoggerProvider provider = iter.next();
        return provider.getPluggableLogger();

    }

    protected PluggableLogger() { }

    public abstract void log(String message);
}