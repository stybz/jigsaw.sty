package bz.sty.main;

import bz.sty.logger.Logger;
import bz.sty.prettylogger.PrettyLogger;

/**
 * From
 *
 * @author Mihail Stoynov
 */
public class Program {
    public static void main(String... args) {
        Logger logger = new PrettyLogger();
//        Logger logger = new Logger();
        logger.log("Hello, World!");
    }
}