package main;

import bz.sty.pluggablelogger.PluggableLogger;
import bz.sty.pluggablelogger.spi.PluggableLoggerProvider;


/**
 * From
 *
 * @author Mihail Stoynov
 */
public class Program {
    public static void main(String... args) {
        PluggableLogger.get().log("Hello, World!");
    }
}