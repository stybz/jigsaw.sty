package bz.sty.superlogger;

import bz.sty.pluggablelogger.PluggableLogger;
import bz.sty.pluggablelogger.spi.PluggableLoggerProvider;

/**
 * From
 *
 * @author Mihail Stoynov
 */
public class SuperLoggerProvider extends PluggableLoggerProvider {

    @java.lang.Override
    public PluggableLogger getPluggableLogger() {
        return new SuperLogger();
    }
}
