package bz.sty.superlogger;

import bz.sty.pluggablelogger.PluggableLogger;

/**
 * From
 *
 * @author Mihail Stoynov
 */
public class SuperLogger extends PluggableLogger {

    public void log(String message) {
        System.out.println("SuperLogger: " + message);
    }
}
