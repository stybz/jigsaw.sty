module bz.sty.superlogger {
        requires bz.sty.pluggablelogger;
        exports bz.sty.superlogger;
        provides bz.sty.pluggablelogger.spi.PluggableLoggerProvider
        with bz.sty.superlogger.SuperLoggerProvider;
        }
